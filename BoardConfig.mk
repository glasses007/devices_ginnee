USE_CAMERA_STUB := true

# inherit from the proprietary version
-include vendor/ginee/706l/BoardConfigVendor.mk

TARGET_ARCH := arm
TARGET_NO_BOOTLOADER := true
TARGET_BOARD_PLATFORM := msm8926
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_VARIANT := cortex-a7
ARCH_ARM_HAVE_TLS_REGISTER := true

TARGET_BOOTLOADER_BOARD_NAME := 706l

BOARD_KERNEL_CMDLINE := console=ttyHSL0,115200,n8 androidboot.console=ttyHSL0 androidboot.hardware=qcom user_debug=31 msm_rtb.filter=0x37
BOARD_KERNEL_BASE := 0x00000000
BOARD_KERNEL_PAGESIZE := 2048
BOARD_CUSTOM_BOOTIMG_MK := device/ginee/706l/mkbootimg.mk

# fix this up by examining /proc/mtd on a running device
TARGET_USERIMAGES_USE_EXT4 := true
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x105c0000
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x105c0000
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 0x105c0000
BOARD_USERDATAIMAGE_PARTITION_SIZE := 0x105c0000
BOARD_FLASH_BLOCK_SIZE := 131072
BOARD_SUPPRESS_EMMC_WIPE := true

TARGET_PREBUILT_KERNEL := device/ginee/706l/kernel

BOARD_HAS_NO_SELECT_BUTTON := true
#TARGET_RECOVERY_FSTAB := device/ginee/706l/recovery/fstab.qcom
#TARGET_RECOVERY_INITRC := device/ginee/706l/recovery/init.rc
#BOARD_CUSTOM_RECOVERY_KEYMAPPING := ../../device/ginee/706l/recovery/recovery_keys.c

BOARD_KERNEL_SEPARATED_DT := true
