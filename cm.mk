## Specify phone tech before including full_phone
$(call inherit-product, vendor/cm/config/gsm.mk)

# Release name
PRODUCT_RELEASE_NAME := 706l

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/ginee/706l/device_706l.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := 706l
PRODUCT_NAME := cm_706l
PRODUCT_BRAND := ginee
PRODUCT_MODEL := 706l
PRODUCT_MANUFACTURER := ginee
